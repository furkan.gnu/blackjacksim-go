package main

import (
	"fmt"
	"sync"
)

type Game struct {
	players          []*Player
	number           uint64
	entry_fee        uint64
	current_max_hand uint8
	round            uint64
	silent           bool
}

func new_game(number uint64, starting_balance uint64, starting_players uint64, entry_fee uint64, silent bool) *Game {
	game := &Game{number: number + 1, entry_fee: entry_fee, round: 1, silent: silent}
	game.players = game.create_players(starting_balance, starting_players)
	return game
}

func (game *Game) create_players(starting_balance uint64, starting_players uint64) []*Player {
	// There will be no dealer
	players := []*Player{}

	for i := uint64(0); i < starting_players; i++ {
		players = append(players, &Player{number: i + 1, balance: starting_balance})
	}
	return players
}

func (game *Game) remove_player(index uint64) {
	game.players[index] = game.players[len(game.players)-1]
	game.players = game.players[:len(game.players)-1]
}

func (game *Game) play(wg *sync.WaitGroup) {
	if !game.silent {
		fmt.Print("##### GAME #", game.number, " IS STARTING #####\n\n")
	}
	for {
		if !game.silent {
			fmt.Print("## Round #", game.round, " is starting ##\n\n")
		}
		game.current_max_hand = 0

		for i := uint64(0); i < uint64(len(game.players)); i++ {
			// Start of the round
			if !game.silent {
				fmt.Print("Player ", game.players[i].number, " joins the round with a balance of: ", game.players[i].balance, "\n")
			}
			game.players[i].balance -= game.entry_fee

			// Clearing players hand and dealing their cards
			game.players[i].clear_cards()
			game.players[i].deal_cards()

			var player_cards = ""
			for k := 0; k < 2; k++ {
				if k == 1 {
					player_cards += ", "
				}
				player_cards += game.players[i].cards[k].name
			}
			if !game.silent {
				fmt.Print("      Dealed cards are: ", player_cards, "\n")
				fmt.Print("      Total value of their hand is: ", game.players[i].hand_value(), "\n\n")
			}
			if game.players[i].hand_value() > game.current_max_hand {
				game.current_max_hand = game.players[i].hand_value()
			}
			for {
				if game.players[i].should_hit() {
					game.players[i].add_card(gen_card())
				} else {
					if game.players[i].hand_value() <= 21 && game.players[i].hand_value() > game.current_max_hand {
						game.current_max_hand = game.players[i].hand_value()
					}
					break
				}
			}
		}

		for i := uint64(0); i < uint64(len(game.players)); i++ {
			// Distributing the rewards
			if game.players[i].hand_value() == game.current_max_hand {
				game.players[i].balance += game.entry_fee * 2
				if !game.silent {
					fmt.Print("Player ", game.players[i].number, " won the round with a hand of ", game.players[i].hand_value(), "\n")
				}
			}

			// Removing players with not enough balance
			for {
				if len(game.players) == 0 {
					break
				}
				if i == uint64(len(game.players)) {
					i--
				}
				if game.players[i].balance < game.entry_fee {
					if !game.silent {
						fmt.Println("Player", game.players[i].number, "has a balance of", game.players[i].balance, "and is no longer able to play.")
					}
					game.remove_player(i)
				} else {
					break
				}
			}
			///////////////////////////////////////////
		}

		if len(game.players) == 1 {
			fmt.Print("||| GAME #", game.number, " ||| The final winner is Player ", game.players[0].number, " with a balance of:", game.players[0].balance, "###\n")
			break
		} else if len(game.players) == 0 {
			fmt.Print("||| GAME #", game.number, " ||| Everyone lost.\n")
			break
		}
		game.round++
	}
	wg.Done()
}
