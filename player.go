package main

type Player struct {
	number  uint64
	balance uint64
	cards   []*Card
}

// Return player hand value, if total > 21
// and player has at least one ace, then
// use the ace with value 1 instead of 11
func (player *Player) hand_value() uint8 {
	var sum uint8 = 0
	var total_ace uint8 = 0
	for i := 0; i < len(player.cards); i++ {
		sum += player.cards[i].value
		if player.cards[i].name == "Ace" {
			total_ace += 1
		}
	}
	if sum > 21 {
		for total_ace > 0 {
			total_ace -= 1
			sum -= 10
			if sum <= 21 {
				break
			}
		}
	}
	return sum
}

// Add card to hand
func (player *Player) add_card(card *Card) {
	player.cards = append(player.cards, card)
}

// Return a bool depending if player should hit
func (player *Player) should_hit() bool {
	hand_value := player.hand_value()

	// Hit or Stand value is going to be calculated
	// semi randomly (means the CPU will not always
	// play the best move) so some rng_value is created
	// to decide if the CPU will Hit or Stand.
	rng_value := 40
	if hand_value < 9 {
		return true
	}
	if hand_value > 19 {
		return false
	} else {
		if hand_value > 14 {
			rng_value += 10
		} else {
			rng_value += 20
		}
	}
	rng_value += (21 - int(hand_value)) * 5
	if gen_rnd(0, rng_value) > 50 {
		return true
	}
	return false
}

func (player *Player) gen_player(number uint64, balance uint64) *Player {
	return &Player{number: number, balance: balance}
}

func (player *Player) clear_cards() {
	player.cards = []*Card{}
}
func (player *Player) deal_cards() {
	player.add_card(gen_card())
	player.add_card(gen_card())
}
