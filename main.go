package main

import (
	"flag"
	"log"
	"sync"
)

func main() {

	// Parsing command line flags
	num_goroutines := flag.Int("g", 0, "Number of goroutines(threads) to run, for a system with 16G of ram, [number of goroutines] * [number of players] sould be <= 100M\nNOTE: GAME WILL PRINT THE OUTPUT RANDOMLY IF RUN WITH MORE THAN 1 THREAD")
	starting_balance := flag.Uint64("b", 0, "Starting balance")
	starting_players := flag.Uint64("p", 0, "Number of players per game")
	entry_fee := flag.Uint64("f", 0, "Entry fee per round")
	number_of_games := flag.Uint64("n", 0, "Number of games to play")
	silent := flag.Bool("s", false, "Only output the winners")
	flag.Parse()
	// Quit if at least one flag is not set
	if *num_goroutines == 0 || *starting_balance == 0 || *starting_players == 0 || *entry_fee == 0 || *number_of_games == 0 {
		log.Fatal("ERROR: ALL THE FLAGS NEED TO BE SET BEFORE RUNNING THE PROGRAM. PLEASE USE blackjacksim-go -h OR blackjacksim-go --help TO GET INFORMATION ABOUT FLAGS")
	} else if *num_goroutines > 1000000 {
		log.Fatal("ERROR: MAXIMUM FOR THE NUMBER OF GOROUTINES IS 1M, GIVE -g FLAG A VALUE <= 1M")
	}

	// Creating a solution to wait for all goroutines to finish
	var wg sync.WaitGroup

	// Running the game
	var tmp_game_number uint64 = 0
	for i := uint64(0); i < *number_of_games/uint64(*num_goroutines); i++ {
		// Vector to store games
		gamevec := []*Game{}
		for k := uint64(0); k < uint64(*num_goroutines); k++ {
			gamevec = append(gamevec, new_game(tmp_game_number, *starting_balance, *starting_players, *entry_fee, *silent))
			tmp_game_number++
		}
		for k := 0; k < len(gamevec); k++ {
			wg.Add(1)
			go gamevec[k].play(&wg)
		}
		wg.Wait()
	}
}
