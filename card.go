package main

type Card struct {
	name  string
	value uint8
}

// Function to return a random card
func gen_card() *Card {
	value := gen_rnd(1, 13)
	name := ""
	switch value {
	case 1:
		name = "A"
		value = 11
	case 2:
		name = "2"
	case 3:
		name = "3"
	case 4:
		name = "4"
	case 5:
		name = "5"
	case 6:
		name = "6"
	case 7:
		name = "7"
	case 8:
		name = "8"
	case 9:
		name = "9"
	case 10:
		name = "10"
	case 11:
		name = "J"
		value = 10
	case 12:
		name = "1"
		value = 10
	case 13:
		name = "K"
		value = 10
	}
	return &Card{name: name, value: value}
}
