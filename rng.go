package main

import (
	"math/rand"
)

func gen_rnd(min int, max int) uint8 {
	// Some error handling to detect which number is the
	// max and which one is the min
	if min > max {
		// Created a temporary variable to swap the values
		// of min and max
		tmp := max

		max = min
		min = tmp
	}
	return uint8(rand.Intn(max-min+1) + min)
}
